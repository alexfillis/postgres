package alexfillis.postgres

import com.mongodb.client.MongoClient
import org.litote.kmongo.KMongo
import org.litote.kmongo.getCollection
import org.litote.kmongo.save
import org.postgresql.ds.PGSimpleDataSource
import javax.sql.DataSource

fun main() {
    val dataSource = pgDataSource()

    val something = doSomething(dataSource)

    val mongo = mongo()

    send(something, mongo)
}

fun mongo(): MongoClient {
    val username = "pomo"
    val password = "secret"
    val host1 = "mongo"
    val port1 = "27017"
    val connectionUrl = "mongodb://$username:$password@$host1:$port1"
    return KMongo.createClient(connectionUrl)
}

fun send(something: Something, mongo: MongoClient) {
    val database = mongo.getDatabase("test")
    val collection = database.getCollection<Something>("something")
    collection.save(something)
}

fun pgDataSource(): PGSimpleDataSource {
    val dataSource = PGSimpleDataSource()
    dataSource.serverNames = arrayOf("postgres")
    dataSource.portNumbers = intArrayOf(0)
    dataSource.databaseName = "custom_db"
    dataSource.user = "custom_user"
    dataSource.password = "custom_pass"
    return dataSource
}

fun doSomething(dataSource: DataSource): Something {
    val connection = dataSource.connection
    val createTableStatement =
        connection.prepareStatement("CREATE TABLE test (businessId VARCHAR NOT NULL, description VARCHAR NOT NULL, age INT)")
    createTableStatement.execute()
    createTableStatement.close()
    val insertStatement = connection.prepareStatement("INSERT INTO test VALUES ('ABC123', 'Big Bun Bakers', 321)")
    insertStatement.execute()
    insertStatement.close()
    val results = connection.prepareStatement("SELECT businessId, description, age FROM test").executeQuery()
    val result = if (results.next()) {
        Something(results.getString(1), results.getString(2), results.getInt(3))
    } else {
        Something("Not", "Found", -1)
    }
    results.close()
    connection.close()
    return result
}

data class Something(val businessId: String, val description: String, val age: Int)

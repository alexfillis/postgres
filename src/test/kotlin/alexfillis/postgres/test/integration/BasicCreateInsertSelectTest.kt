package alexfillis.postgres.test.integration

import alexfillis.postgres.doSomething
import alexfillis.postgres.mongo
import alexfillis.postgres.pgDataSource
import alexfillis.postgres.send
import kotlin.test.Test

class BasicCreateInsertSelectTest {
    @Test
    fun `name name`() {
        val dataSource = pgDataSource()

        val something = doSomething(dataSource)

        val mongo = mongo()

        send(something, mongo)
    }
}